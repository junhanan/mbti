from django.contrib import admin
from .models import MBTIType
# Register your models here.

@admin.register(MBTIType)
class MBTITypeAdmin(admin.ModelAdmin):
    list_display =(
        "type_name",
        "type_description",
        "type_image",
        "traits",
        "hard_facts"
    )
