from django.apps import AppConfig


class MainMbtiTypeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main_mbti_type'
