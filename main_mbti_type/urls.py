from django.urls import path
from .views import index, mbti_detail

app_name = "main_page"

urlpatterns = [
    path("", index, name="index"),
    path("mbti/<str:type_name>/", mbti_detail, name="mbti_detail"),
]
