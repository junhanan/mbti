from django.db import models
from django.conf import settings

# Create your models here.

class MBTIType(models.Model):
    type_name = models.CharField(max_length=4)
    type_description = models.TextField()
    type_image = models.ImageField(upload_to="images/")
    traits = models.TextField(default="", blank=True, null=True)
    hard_facts = models.TextField(default="", blank=True, null=True)

    def __str__(self):
        return self.type_name

# class Compatibility(models.Model):
#     mbti_type1 = models.ForeignKey(
#         MBTIType,
#         related_name = "compatibility_type1",
#         on_delete=models.CASCADE
#     )
#     mbti_type2 = models.ForeignKey(
#         MBTIType,
#         related_name = "compatibility_type2",
#         on_delete=models.CASCADE
#     )
#     compatibility_score = models.IntegersField()
