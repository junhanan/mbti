# Generated by Django 4.2 on 2023-05-04 18:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_mbti_type', '0003_mbtitype_traits'),
    ]

    operations = [
        migrations.AddField(
            model_name='mbtitype',
            name='hard_facts',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='mbtitype',
            name='traits',
            field=models.TextField(blank=True, default='', null=True),
        ),
        migrations.AlterField(
            model_name='mbtitype',
            name='type_image',
            field=models.ImageField(upload_to='images/'),
        ),
    ]
